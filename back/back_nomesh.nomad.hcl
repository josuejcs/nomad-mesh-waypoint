variable "image" {
  default = "hcdcanadillas/pydemo-back"
}
variable "image_version" {
  default = "v1.2"
}
variable "datacenter" {
  default = "dc1"
}
variable "client" {}

job "back" {
  datacenters = ["${var.datacenter}"]

  group "back" {
    network {
      port "back" {
        static = 9090
        to = 9090
      }
      # To use Consul Connect use "bridge" mode, but it is not supported in MacOS
      mode = "bridge"
    }
    constraint {
      operator  = "regexp"
      attribute = "$${node.unique.name}"
      value     = "${var.client}"
    }

    task "backend" {
      driver = "docker"

      config {
        image = "${var.image}:${var.image_version}"
        ports = ["back"]
      }
      env {
        // For URL service
        PORT = "9090"
      }
    }
  }
}