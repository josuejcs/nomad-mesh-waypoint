project = "pydemo-back-remote"


variable "nomad_dc" {
  default = "dc1"
}

variable "nomad_job" {
  default = "back"
}

variable "arch" {
  # default = "amd64"
}

# variable "runner" {
#   # default = ""
# }

variable "docker_user" {
  # default = ""
}

variable "docker_token" {
  # default = ""
}



# Please, use a global scope configuration to set NOMAD_ADDR for the runner
# e.g.: waypoint config set -runner -scope global NOMAD_ADDR="http://<nomad_addr_accesible_from_container>:4646"

runner {
  profile = "nomad-bootstrap-profile"
}


app "back" {
  path = "${path.project}"
  url {
    auto_hostname = false
  }

  build {
    // use "docker" {}
    // registry {
    //   use "docker" {
    //     image = "hcdcanadillas/bk8s-back"
    //     tag   = "v1-${var.arch}"
    //     local = true

    //     # auth {
    //     #   username = var.docker_user
    //     #   identityToken = var.docker_token
    //     # }
    //     username = var.docker_user
    //     password = var.docker_token
    //   }
    // }
    use docker-pull {
      image = "hcdcanadillas/bk8s-back"
      tag   = "v1-${var.arch}"
      auth {
        username = var.docker_user
        password = var.docker_token
      }
    }
  }

  deploy {
    # use "nomad" {
    #   datacenter = "dcanadillas"
    #   static_environment = {
    #     PORT = "9090"
    #   }
    #   service_port = 9090
    #   replicas = 1
    # }
    // use "exec" {
    //   # command = ["nomad", "job", "run", templatefile("${path.app}/back.nomad.tpl")]
    //   command = ["nomad", "job", "run", "<TPL>"]
    //   template {# 
    //     path = "${path.app}/back.nomad.tpl"
    //   }
    // }
    use "nomad-jobspec" {
      jobspec = templatefile("${path.app}/${var.nomad_job}.nomad.tpl",{
        datacenter = var.nomad_dc
      })
    }
  }
}
 
