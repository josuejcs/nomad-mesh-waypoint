job "front" {
  datacenters = ["${datacenter}"]
  namespace = "${namespace}"
  group "front" {
    network {
      port "front" {}
      # To use Consul Connect use "bridge" mode, but it is not supported in MacOS
      mode = "bridge"
    }
    constraint {
      operator  = "regexp"
      attribute = "$${node.unique.name}"
      value     = "${client_constraint}"
    }
    update {
      canary = 1
      max_parallel = 1
      auto_promote = false
    }
    service {
      name = "frontend"
      tags = ["frontend", "python", "${artifact.tag}"]
      meta {
        %{ if cloud == "gcp" }
        zone = "$${attr.platform.gce.zone}"
        %{ endif }
        %{ if cloud == "azure" }
        location = "$${attr.platform.azure.location}"
        azure_vm = "$${attr.unique.platform.azure.name}"
        resource_group = "$${attr.platform.azure.resource-group}"
        %{ endif }
        %{ if cloud == "aws" }
        ami_id = "$${attr.platform.aws.ami-id}"
        %{ endif }
        hostname = "$${attr.unique.hostname}"
      }
      port = "front"
      connect {
        sidecar_service {
          proxy {
            upstreams {
              destination_name = "backend"
              local_bind_port = 9090
            }
          }
        }
      }
    }

    task "front" {
      driver = "docker"

      config {
        image = "${artifact.image}:${artifact.tag}"
        ports = ["front"]
        force_pull = true
      }
      env {
        %{ for k,v in entrypoint.env ~}
        ${k} = "${v}"
        %{ endfor ~}
        PORT = "$$${NOMAD_PORT_front}"
        # Host IP:Port for the given service when defined as a Consul Connect
        BACKEND_URL = "http://$$${NOMAD_UPSTREAM_ADDR_backend}"
      }
    }
  }
}