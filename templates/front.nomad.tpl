job "front" {
  datacenters = ["${datacenter}"]

  group "front" {
    network {
      port "front" {}
      # mode = "bridge"
    }
    service {
      name = "front"
      tags = ["frontend", "python"]
      port = "front"
    }

    task "front" {
      driver = "docker"

      config {
        image = "${artifact.image}:${artifact.tag}"
        ports = ["front"]
      }
      env {
        %{ for k,v in entrypoint.env ~}
        ${k} = "${v}"
        %{ endfor ~}
        PORT = "$${NOMAD_PORT_front}"
        BACKEND_URL = "http://$${NOMAD_IP_front}:9090"
      }
    }
  }
}