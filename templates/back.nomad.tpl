job "back" {
  datacenters = ["${datacenter}"]

  group "back" {
    network {
      port "back" {
        static = 9090
        to = 9090
      }
      # To use Consul Connect use "bridge" mode, but it is not supported in MacOS
      # mode = "bridge"
    }
    service {
      name = "back"
      tags = ["backend", "python"]
      port = 9090
    }


    task "back" {
      driver = "docker"

      config {
        image = "${artifact.image}:${artifact.tag}"
        ports = ["back"]
      }
      env {
        %{ for k,v in entrypoint.env ~}
        ${k} = "${v}"
        %{ endfor ~}

        PORT = "$${NOMAD_PORT_back}"
      }
    }
  }
}