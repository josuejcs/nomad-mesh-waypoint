job "ingress-gateway" {

  group "gateway" {
    network {
      mode = "bridge"
      port "inbound" {
        static = 8080
        to     = 8080
      }
    }
    service {
      name = "ingress-gateway"
      port = "8080"

      connect {
        gateway {
          proxy {}

          ingress {
            listener {
              port = 8080
              protocol = "http"
              service {
                name  = "frontend"
                hosts = [ "*" ]
              }
            }
          }
        }
      }
    }
  }
}
