job "docker-registry" {
  datacenters = ["dc1"]
  group "cache" {
    network {
      port "reg" {
        static = 5000
        to = 5000
      }
    }

    volume "docker-registry" {
      type      = "host"
      source    = "docker-registry"
      read_only = false
    }

    task "registry" {
      driver = "docker"

      config {
        image          = "registry:2"
        ports          = ["reg"]
        auth_soft_fail = true
      }
      volume_mount {
        volume      = "docker-registry"
        destination = "/data"
        propagation_mode = "private"
      }
      env {
        REGISTRY_STORAGE_FILESYSTEM_ROOTDIRECTORY = "/data"
        // REGISTRY_HTTP_ADDR = "0.0.0.0:443"
        REGISTRY_HTTP_TLS_CERTIFICATE = "/data/cert.crt"
        REGISTRY_HTTP_TLS_KEY = "/certs/cert.key"
      }

      identity {
        env  = true
        file = true
      }

      resources {
        cpu    = 250
        memory = 256
      }
    }
  }
}