job "front" {
  datacenters = ["${datacenter}"]

  group "front" {
    network {
      port "front" {}
      # To use Consul Connect use "bridge" mode, but it is not supported in MacOS
      mode = "bridge"
    }
    service {
      name = "front"
      tags = ["frontend", "python"]
      port = "front"
      connect {
        sidecar_service {
          proxy {
            upstreams {
              destination_name = "back"
              local_bind_port = 9090
            }
          }
        }
      }
    }

    task "front" {
      driver = "docker"

      config {
        image = "${artifact.image}:${artifact.tag}"
        ports = ["front"]
      }
      env {
        %{ for k,v in entrypoint.env ~}
        ${k} = "${v}"
        %{ endfor ~}
        PORT = "$${NOMAD_PORT_front}"
        # Host IP:Port for the given service when defined as a Consul Connect
        BACKEND_URL = "http://$${NOMAD_UPSTREAM_ADDR_back}"
      }
    }
  }
}