variable "image" {
  default = "hcdcanadillas/pydemo-front"
}
variable "image_version" {
  default = "v1.3"
}
variable "datacenter" {
  default = "dc1"
}
variable "client" {}

job "front" {
  datacenters = ["${var.datacenter}"]

  group "front" {
    network {
      port "front" {
        static = 8080
        to = 8080
      }
      mode = "bridge"
    }
    constraint {
      operator  = "regexp"
      attribute = "$${node.unique.name}"
      value     = "${var.client}"
    }
    task "frontend" {
      driver = "docker"

      config {
        image = "${var.image}:${var.image_version}"
        ports = ["front"]
        force_pull = true
      }
      env {
        PORT = "$${NOMAD_PORT_front}"
        BACKEND_URL = "http://localhost:9090"
      }
    }
  }
}