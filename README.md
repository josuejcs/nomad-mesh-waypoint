# Using Consul for Progressive Delivery and Zero-Trust

## Requirements

* An x86 Linux machine or VM (MacOS cannot handle [Consul Service Mesh features with Nomad](https://www.nomadproject.io/docs/integrations/consul-connect) because of Linux network namespaces requirements ) to instal Nomad and Consul
* Waypoint server installation. You can install it in the same Nomad server you are deploying or locally using Docker
* [Waypoint binary](https://www.waypointproject.io/downloads) installed. Download it and put it in an executable path, or using a package manager
* Docker installation up and running

## WIP...
...