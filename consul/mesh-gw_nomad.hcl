variable "datacenter" {
  default = "dc1"
}

job "consul-mgw" {
  datacenters = ["${var.datacenter}"]
  group "mesh-gateway" {
    network {
      mode = "host"
      port "mesh" {
        static = 8443
      }
    }
    service {
      name = "mesh-gateway"
      port = "mesh"
      tags = ["$${attr.unique.platform.gce.network.hashistack-gcp-demo-network.external-ip.0}"]
      tagged_addresses {
        lan = "$${attr.unique.platform.gce.network.hashistack-gcp-demo-network.ip}"
        wan = "$${attr.unique.platform.gce.network.hashistack-gcp-demo-network.external-ip.0}"
      }

      connect {
        gateway {
          mesh {}
        }
      }
    }
  }
}